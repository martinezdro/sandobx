
PImage img_input, img_output;
color black, white;
int height1, width1, height2, width2; 
int zoom;
int new_valor, valor1, valor2, valor3, valor4;
int red, green, blue;


float temp_x, temp_y, u, v;
int x1, x2, x3, x4, y1, y2, y3, y4;
color c1, c2, c3, c4, new_color;

void setup(){

  size(720, 550);
  background(245);


  img_input = loadImage("lena128gray.jpg");
  zoom = 4;
  width1=img_input.width;
  height1=img_input.height;

  width2=zoom * width1;
  height2=zoom * height1;


  /*

  width1=50;
  height1=50;
  zoom = 4;

  width2=zoom * width1;
  height2=zoom * height1;

  black = color(0);
  white = color(255);
  
  img_input = createImage(width1, height1, RGB);
  img_input.loadPixels();

  for (int i=0; i<width1; i++){    
    for (int j=0; j<height1; j++){
      if (i<=j){
        img_input.set(i,j, black);
      } else {    
        img_input.set(i,j, white);
      }
    }
  }

  img_input.updatePixels();

  */

  
  img_output = createImage(width2, height2, RGB);
  img_output.loadPixels();

  for (int i=0; i<width2; i++){
    
    for (int j=0; j<height2; j++){

      temp_x = i * (width1/(width2+0.0));
      temp_y = j * (height1/(height2+0.0));

      x1 = int(temp_x);
      y1 = int(temp_y);

      x2 = x1;
      y2 = y1 + 1;

      x3 = x1 + 1;
      y3 = y1; 

      x4 = x1 + 1;
      y4 = y1 + 1;

      u = temp_x - x1;
      v = temp_y - y1;

      if (x4>=width1){
        x4 = width1 - 1;
        x3 = x4;
        x1 = x4 - 1;
        x2 = x4 - 1;
      }

      if (y4>=height1){
        y4 = height1 - 1;
        y2 = y4;
        y1 = y4 - 1;
        y3 = y4 - 1;
      }


      red = int(red(img_input.get(x1,y1)));
      green = int(green(img_input.get(x1,y1)));
      blue = int(blue(img_input.get(x1,y1)));
      valor1 = int((red + green + blue)/3);

      red = int(red(img_input.get(x2,y2)));
      green = int(green(img_input.get(x2,y2)));
      blue = int(blue(img_input.get(x2,y2)));
      valor2 = int((red + green + blue)/3);

      red = int(red(img_input.get(x3,y3)));
      green = int(green(img_input.get(x3,y3)));
      blue = int(blue(img_input.get(x3,y3)));
      valor3 = int((red + green + blue)/3);

      red = int(red(img_input.get(x4,y4)));
      green = int(green(img_input.get(x4,y4)));
      blue = int(blue(img_input.get(x4,y4)));
      valor4 = int((red + green + blue)/3);

      new_valor = int((1-u)*(1-v)*valor1 + u*(1-v)*valor2 + (1-u)*v*valor3 + u*v*valor4);     
 
      img_output.set(i,j,color(new_valor));

    }   

  }

  img_output.updatePixels();
  
}


void draw(){
  image(img_input, 0, 0);

  image(img_output, width1+20, 0);
}

