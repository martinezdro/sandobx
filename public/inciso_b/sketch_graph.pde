
// definicion de clases

class Vertex {
  String name;
  int posX, posY;

  Vertex(String name){
    this.name = name;
    this.posX = -1;
    this.posY = -1;
  }

  void generatePosition(int minX, int maxX, int minY, int maxY){    
    setPosX(int(random(minX, maxX)));
    setPosY(int(random(minY, maxY)));
  }

  int getPosX(){
    return this.posX;
  }

  int getPosY(){
    return this.posY;
  }

  String getName(){
    return this.name;
  }

  void setPosX(int value){
    this.posX = value;
  }

  void setPosY(int value){
    this.posY = value;
  }

}


class Edge {
  String vertI, vertJ;
  boolean drawn; // ya se dibujo

  Edge(String vertI, String vertJ){
    this.vertI = vertI;
    this.vertJ = vertJ;
    setDrawn(false);
  }

  void setDrawn(boolean response){
    this.drawn = response;
  }

  String getVertI(){
    return this.vertI;
  }

  String getVertJ(){
    return this.vertJ;
  }

  boolean wasDrawn(){
    return this.drawn;
  }
}


// definicion de variables
ArrayList<Vertex> vertexes;
ArrayList<Edge> edges;
Edge objEdge1, objEdge2;
Vertex objVertI, objVertJ, objVertP, objVertQ;
int posX1, posY1, posX2, posY2;

String[] lines, element, lines2;
String datFile, infoFile;
String cadena, cadena_i, cadena_j;
String separator;
int width, height;
int background_color, point_color, line_color;
int seedValue;
int residual; // para que no incluyes los limites de la pantalla
boolean flag, control;
int i, max, j;
int counter;
PFont f;

void setup(){
  size(800, 600);
  width= 600;
  height = 400;
  residual = 50;
  background_color = 0;
  point_color = 255;
  line_color = 153;
  seedValue = 0;
  separator = "|";
  counter = 0;

  f = createFont("Arial", 10, true);
  
  randomSeed(seedValue);

  vertexes = new ArrayList<Vertex>();
  edges = new ArrayList<Edge>();
  
  datFile = "stars.dat"; // leyendo estrellas
  infoFile = "info.dat"; // leyendo posiciones

  try {
    lines = loadStrings(datFile);
    println("there are " + lines.length + " lines");
    
  } catch (Exception e) {
    e.printStackTrace();
    lines = null;
  }


  try {
    lines2 = loadStrings(infoFile);
    
  } catch (Exception e) {
    e.printStackTrace();
    lines2 = null;
  }


  if (lines != null && lines2!=null) {

    for (int i = 1 ; i < lines.length; i++) {
      element = split(lines[i], separator);
      cadena_i = element[3].strip();
      cadena_j = element[4].strip();

      if (isVertexInList(cadena_i)==false){
        vertexes.add(new Vertex(cadena_i));       

      }      

      if (isVertexInList(cadena_j)==false){
        vertexes.add(new Vertex(cadena_j));
        
      }

      if (isEdgeInList(cadena_i, cadena_j)==false){
        edges.add(new Edge(cadena_i, cadena_j));
        
      }
    
    }


    control = loadingPositions(); // leo info de posiciones
    

    if (control==false){
      // no se encontró info de algún vértice...
      lines2 = null;

    } else {
      
      flag = false;
      setNotDrawn(); // seteo drawn a false a todas las aristas


      i = 0;
      max = edges.size();

      while (i<max){
        objEdge1 = edges.get(i);
          
        cadena_i = objEdge1.getVertI();;
        cadena_j = objEdge1.getVertJ();;

        objVertI = getVertex(cadena_i);
        objVertJ = getVertex(cadena_j);


        // comparo con las aristas 'visitadas' si intersectan
        // si es verdadero, no puedo graficar. sino, avanzo

        j = 0;
        while (j<i){          
          objEdge2 = edges.get(j);
          
          cadena_i = objEdge2.getVertI();;
          cadena_j = objEdge2.getVertJ();;

          objVertP = getVertex(cadena_i);
          objVertQ = getVertex(cadena_j);


          if (objVertI.getPosX()!=objVertP.getPosX() && objVertI.getPosY()!=objVertP.getPosY() && objVertI.getPosX()!=objVertQ.getPosX() && objVertI.getPosY()!=objVertQ.getPosY() && objVertJ.getPosX()!=objVertP.getPosX() && objVertJ.getPosY()!=objVertP.getPosY() && objVertJ.getPosX()!=objVertQ.getPosX() && objVertJ.getPosY()!=objVertQ.getPosY()){


            if (doIntersect(objVertI, objVertJ, objVertP, objVertQ)==true){

              j = max + 10; // salir. hay interseccion de lineas              
            
            }

          }

          j = j+1;
        
        }

        if (j<max + 10){
          // marco objEdge1 como 'dibujada'

          objEdge1.setDrawn(true);
          i = i + 1;

        } else {
          i = max + 10;
        }        

      }


      if (i< max + 10){      
        print("Fin!");
        flag = true; // se puede graficar

      } 

    }  

  }
  
}

 
void draw() { 
  noLoop();
  textFont(f, 10);
  fill(128);


  if (lines == null || lines2 == null) {
    println("Oops! Error con los archivos. Reintente");

  } else {
    if (flag==true){
      drawGraph();
      println("grafo en pantalla");
    
    } else {
      println("Oops! No se pudo graficar x colision de aristas");
    }
  } 

} 


// módulos de soporte

boolean loadingPositions(){
  boolean response = false;
  int i, posX, posY;
  String name;
  Vertex objVertex;
  
  i = 1;
  while (i<lines2.length){
    element = split(lines2[i], separator);
    
    name = element[2].strip();
    posX = int(element[3].strip());
    posY = int(element[4].strip());

    objVertex = getVertex(name);    

    if (objVertex!=null){
      objVertex.setPosX(posX);
      objVertex.setPosY(posY);
    
    } else {
      i = lines2.length + 10;
    }

    i = i + 1;
  }

  if (i<lines2.length + 10){
    response = true;
  }  

  return response;

}


void setNotDrawn(){
  for (Edge objEdge : edges){
    objEdge.setDrawn(false);    

  }
}

boolean isVertexInList(String cadena){
  boolean response = false;
  int i = 0;
  int max = vertexes.size();
  Vertex objVertex;

  while (i<max){
    objVertex = vertexes.get(i);

    if (objVertex.getName().equals(cadena)){
      i = max + 1;
      response = true;

    } else {
      i = i +1;
    }
  
  }

  return response;

}


boolean isEdgeInList(String cadena_i, String cadena_j){
  boolean response = false;  
  int i = 0;
  int max = edges.size();
  Edge objEdge;

  while (i<max){
    objEdge = edges.get(i);

    if ((objEdge.getVertI().equals(cadena_i) & objEdge.getVertJ().equals(cadena_j)) | (objEdge.getVertI().equals(cadena_j) & objEdge.getVertJ().equals(cadena_i))){
      i = max + 1;
      response = true;
    } else {
      i = i + 1;
    }

  }

  return response; 

}


Vertex getVertex(String cadena){
  Vertex objVertex = null;
  int i = 0;
  int max = vertexes.size();

  while (i<max){
    objVertex = vertexes.get(i);

    if (objVertex.getName().equals(cadena)){
      i = max + 10;
    } else {
      i = i + 1;
    }

  }

  if (i>=max + 10){
    return objVertex;
  } else {
    return null;
  }

}

// dados 3 puntos colineales p, q, r, chequeamos si q esta
// en el segmento pr
boolean onSegment(Vertex p, Vertex q, Vertex r){
  boolean response = false;

  if (q.getPosX() <= max(p.getPosX(), r.getPosX()) && q.getPosX() >= min(p.getPosX(), r.getPosX()) && q.getPosY() <= max(p.getPosY(), r.getPosY()) && q.getPosY() >= min(p.getPosY(), r.getPosY())){  
    response = true;    
  }
  
  return response;
}

// devuelve la orientacion de la tripleta (p, q, r).
// 0-> p, q y r son colineales
// 1-> sentido agujas del reloj
// 2-> sentido contrario agujas del reloj
int orientation (Vertex p, Vertex q, Vertex r){
  int response;
  int value;

  value = (q.getPosY() - p.getPosY()) * (r.getPosX() - q.getPosX()) - (q.getPosX() - p.getPosX()) * (r.getPosY() - q.getPosY());

  if (value==0){
    response = 0;
  } else {
    if (value>0){
      response = 1;
    } else {
      response = 2;
    }
  }

  return response;

}

// devuelve true si p1q1 intersecta con p2q2
boolean doIntersect(Vertex p1, Vertex q1, Vertex p2, Vertex q2){
  boolean response = false;
  int or1, or2, or3, or4;

  or1 = orientation(p1, q1, p2);
  or2 = orientation(p1, q1, q2);
  or3 = orientation(p2, q2, p1);
  or4 = orientation(p2, q2, q1);

  if (or1!=or2 && or3!=or4){
    response = true;

    println("A");

  }
  
  if (or1==0 && onSegment(p1,p2,q1)){
    response = true;
  
  }

  if (or2==0 && onSegment(p1,q2,q1)){
    response = true;  
    
  }

  if (or3==0 && onSegment(p2,p1,q2)){
    response = true;   
  
  }

  if (or4==0 && onSegment(p2,q1,q2)){
    response = true;    

  }

  return response;

}


void drawGraph(){

  for (Edge objEdge : edges){    
  
    cadena_i = objEdge.getVertI();;
    cadena_j = objEdge.getVertJ();;

    objVertI = getVertex(cadena_i);
    objVertJ = getVertex(cadena_j);

    line(objVertI.getPosX(), objVertI.getPosY(), objVertJ.getPosX(), objVertJ.getPosY());

    text(objVertI.getName(), objVertI.getPosX()+5, objVertI.getPosY() + 5);
    text(objVertJ.getName(), objVertJ.getPosX()+5, objVertJ.getPosY() + 5);

  }
  
}
